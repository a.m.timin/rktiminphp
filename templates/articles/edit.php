<?php include __DIR__.'/../header.html';?>
    <div class="edit">
        <h3>Редактирование статьи</h3>
        <form action="" method="post" class="form">
            <label for="">Заголовок статьи</label>
            <input type="text" required name="name" value="<?=$article->getName()?>">
            <br>
            <label for="">Текст статьи</label>
            <textarea type="text" required name="text"><?=$article->getText()?></textarea>
            <button class="button">Сохранить</button>
        </form>
    </div>
<?php include __DIR__.'/../footer.html';

