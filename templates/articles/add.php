<?php include __DIR__.'/../header.html';?>
    <div class="edit">
        <h3>Новая статья</h3>
        <form action="" method="post" class="form">
            <label for="">Заголовок статьи</label>
            <input type="text" required name="name" placeholder="Статья #1" minlength="3">
            <br>
            <label for="">Текст статьи</label>
            <textarea type="text" required name="text" placeholder="Бла бла бла" minlength="3"></textarea>
            <button class="button">Создать</button>
        </form>
    </div>
<?php include __DIR__.'/../footer.html';

