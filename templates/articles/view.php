<?php include __DIR__.'/../header.html';?>
    <h2><?= $article->getName();?></h2>
    <p><?= $article->getText();?></p>
    <hr>
    <div>
        <a href="/OOP/www/article/<?=$article->getId()?>/edit" class="button">Редактировать</a>
        <a href="/OOP/www/article/<?=$article->getId()?>/delete" class="button">Удалить</a>
    </div>
    <h3>Оставить комментарий</h3>
    <form class="form" action="/OOP/www/article/<?=$article->getId()?>/comments" method="post">
        <input type="text" name="article_id" value="<?=$article->getId()?>" class="hidden">
        <textarea name="text" cols="30" rows="10"></textarea>
        <button class="button">Отправить</button>
    </form>
    <?php if ($comments !== []) { ?>
    <h3>Комментарии</h3>
    <ul>
        <?php foreach($comments as $comment): ?>
            <hr>
            <li class="comment" id="comment<?=$comment->getId()?>">
                <p class="comment__text"><?=$comment->getText()?></p>
                <p class="comment__author">Автор: <?=$comment->getAuthor()->getName()?></p>
                <div class="comment__functions">
                    <a href="/OOP/www/comments/<?=$comment->getId()?>/edit" class="button">Редактировать</a>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php } ?>
<?php include __DIR__.'/../footer.html';
